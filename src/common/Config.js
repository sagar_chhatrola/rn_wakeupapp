/** @format */

import Images from "./Images";
import Constants from "./Constants";
import Icons from "./Icons";

const consumerKey = "ck_a60e7249075678a5c25a01a33849dcc28a503744";
const consumerSecret = "cs_9925c5e29e8a6e2512a3575ea8aa6867114e0274";

export default {
  /**
   * Step 1: change to your website URL and the wooCommerce API consumeKey
   */
  WooCommerce: {
    url: "https://shop.wakeupprogramkingsfoundation-rdc.com/",
    consumerKey,
    consumerSecret,
  },

  /**
     Step 2: Setting Product Images
     - ProductSize: Explode the guide from: update the product display size: https://mstore.gitbooks.io/mstore-manual/content/chapter5.html
     The default config for ProductSize is disable due to some problem config for most of users.
     If you have success config it from the Wordpress site, please enable to speed up the app performance
     - HorizonLayout: Change the HomePage horizontal layout - https://mstore.gitbooks.io/mstore-manual/content/chapter6.html
       Update Oct 06 2018: add new type of categories
       NOTE: name is define value --> change field in Language.js
       Moved to AppConfig.json
     */
  ProductSize: {
    enable: true,
  },

  HomeCategories: [
    {
      category: 4218,
      image: require("@images/categories_icon/ic_shorts.png"),
      colors: ["#4facfe", "#00f2fe"],
      label: "Men's Clothing",
    },
    {
      category: 4219,
      image: require("@images/categories_icon/ic_tshirt.png"),
      colors: ["#43e97b", "#38f9d7"],
      label: "T-Shirts",
    },
    {
      category: 4222,
      image: require("@images/categories_icon/ic_car.png"),
      colors: ["#fc3103", "#fee140"],
      label: "Cars",
    },
    {
      category: 4220,
      image: require("@images/categories_icon/ic_gadget.png"),
      colors: ["#fa709a", "#fee140"],
      label: "Electronics",
    },
    {
      category: 4219,
      image: require("@images/categories_icon/ic_dress.png"),
      colors: ["#7F00FF", "#E100FF"],
      label: "Women's Clothing",
    },
    {
      category: 4221,
      image: require("@images/categories_icon/ic_book.png"),
      colors: ["#30cfd0", "#330867"],
      label: "Books",
    },
    {
      category: 4497,
      image: require("@images/categories_icon/ic_event.png"),
      colors: ["#ff7f00", "#330845"],
      label: "Events",
    },
    {
      category: 4315,
      image: require("@images/categories_icon/ic_food.png"),
      colors: ["#fk7k00", "#330867"],
      label: "Food",
    },
    {
      category: 4223,
      image: require("@images/categories_icon/ic_hotel.png"),
      colors: ["#fcba03", "#330867"],
      label: "Hotels",
    },
    {
      category: 4225,
      image: require("@images/categories_icon/ic_hotel.png"),
      colors: ["#fc03ba", "#330867"],
      label: "House & Garden",
    },
    {
      category: 4226,
      image: require("@images/categories_icon/ic_hotel.png"),
      colors: ["#fc034e", "#330867"],
      label: "Real Estate",
    },
    {
      category: 4224,
      image: require("@images/categories_icon/ic_food.png"),
      colors: ["#03fc0b", "#330867"],
      label: "Restaurant",
    },
  ],
  HomeCategories_FR: [
    {
      category: 4218,
      image: require("@images/categories_icon/ic_shorts.png"),
      colors: ["#4facfe", "#00f2fe"],
      label: "Vêtements pour hommes",
    },
    {
      category: 4219,
      image: require("@images/categories_icon/ic_tshirt.png"),
      colors: ["#43e97b", "#38f9d7"],
      label: "T-Shirts",
    },
    {
      category: 4222,
      image: require("@images/categories_icon/ic_car.png"),
      colors: ["#fc3103", "#fee140"],
      label: "Voitures",
    },
    {
      category: 4220,
      image: require("@images/categories_icon/ic_gadget.png"),
      colors: ["#fa709a", "#fee140"],
      label: "Électronique",
    },
    {
      category: 4219,
      image: require("@images/categories_icon/ic_dress.png"),
      colors: ["#7F00FF", "#E100FF"],
      label: "Vêtements pour femmes",
    },
    {
      category: 4221,
      image: require("@images/categories_icon/ic_book.png"),
      colors: ["#30cfd0", "#330867"],
      label: "Livres",
    },
    {
      category: 4497,
      image: require("@images/categories_icon/ic_event.png"),
      colors: ["#ff7f00", "#330845"],
      label: "Événements",
    },
    {
      category: 4315,
      image: require("@images/categories_icon/ic_food.png"),
      colors: ["#fk7k00", "#330867"],
      label: "Aliments",
    },
    {
      category: 4223,
      image: require("@images/categories_icon/ic_hotel.png"),
      colors: ["#fcba03", "#330867"],
      label: "Hôtels",
    },
    {
      category: 4225,
      image: require("@images/categories_icon/ic_hotel.png"),
      colors: ["#fc03ba", "#330867"],
      label: "Maison & Jardin",
    },
    {
      category: 4226,
      image: require("@images/categories_icon/ic_hotel.png"),
      colors: ["#fc034e", "#330867"],
      label: "Immobilier",
    },
    {
      category: 4224,
      image: require("@images/categories_icon/ic_food.png"),
      colors: ["#03fc0b", "#330867"],
      label: "Restaurant",
    },
  ],
  
  /**
     step 3: Config image for the Payment Gateway
     Notes:
     - Only the image list here will be shown on the app but it should match with the key id from the WooCommerce Website config
     - It's flexible way to control list of your payment as well
     Ex. if you would like to show only cod then just put one cod image in the list
     * */
  Payments: {
    cod: require("@images/payment_logo/cash_on_delivery.png"),
    paypal: require("@images/payment_logo/dpo.png"),
    stripe: require("@images/payment_logo/stripe.png"),
    woocommerce_dpo: require("@images/payment_logo/dpo.png"),
    maxicashgateway: require("@images/payment_logo/maxicash.png"),
  },

  /**
     Step 4: Advance config:
     - showShipping: option to show the list of shipping method
     - showStatusBar: option to show the status bar, it always show iPhoneX
     - LogoImage: The header logo
     - LogoWithText: The Logo use for sign up form
     - LogoLoading: The loading icon logo
     - appFacebookId: The app facebook ID, use for Facebook login
     - CustomPages: Update the custom page which can be shown from the left side bar (Components/Drawer/index.js)
     - WebPages: This could be the id of your blog post or the full URL which point to any Webpage (responsive mobile is required on the web page)
     - intro: The on boarding intro slider for your app
     - menu: config for left menu side items (isMultiChild: This is new feature from 3.4.5 that show the sub products categories)
     * */
  shipping: {
    visible: true,
    zoneId: 1, // depend on your woocommerce
    time: {
      free_shipping: "4 - 7 Days",
      flat_rate: "1 - 4 Days",
      local_pickup: "1 - 4 Days",
    },
  },
  showStatusBar: true,
  LogoImage: require("@images/logo-main.png"),
  LogoWithText: require("@images/logo_with_text.png"),
  LogoLoading: require("@images/logo.png"),

  showAdmobAds: false,
  AdMob: {
    deviceID: "pub-2101182411274198",
    rewarded: "ca-app-pub-2101182411274198/5096259336",
    interstitial: "ca-app-pub-2101182411274198/8930161243",
    banner: "ca-app-pub-2101182411274198/4100506392",
  },
  appFacebookId: "257846575563630",
  CustomPages: { contact_id: 10941 },
  WebPages: { marketing: "https://shop.wakeupprogramkingsfoundation-rdc.com/" },

  intro: [
    {
      key: "page1",
      title: "Welcome to Wakeupkings!",
      text: "Get the hottest fashion by trend and season right on your pocket.",
      icon: "ios-basket",
      colors: ["#0FF0B3", "#036ED9"],
    },
    {
      key: "page2",
      title: "Secure Payment",
      text: "All your payment infomation is top safety and protected",
      icon: "ios-card",
      colors: ["#13f1fc", "#0470dc"],
    },
    {
      key: "page3",
      title: "High Performance",
      text: "Saving your value time and buy product with ease",
      icon: "ios-finger-print",
      colors: ["#b1ea4d", "#459522"],
    },
  ],

  /**
   * Config For Left Menu Side Drawer
   * @param goToScreen 3 Params (routeName, params, isReset = false)
   * BUG: Language can not change when set default value in Config.js ==> pass string to change Languages
   */
  menu: {
    // has child categories
    isMultiChild: true,
    // Unlogged
    listMenuUnlogged: [
      {
        text: "Login",
        routeName: "LoginScreen",
        params: {
          isLogout: false,
        },
        icon: Icons.MaterialCommunityIcons.SignIn,
      },
    ],
    // user logged in
    listMenuLogged: [
      {
        text: "Logout",
        routeName: "LoginScreen",
        params: {
          isLogout: true,
        },
        icon: Icons.MaterialCommunityIcons.SignOut,
      },
    ],
    // Default List
    listMenu: [
      {
        text: "Shop",
        routeName: "Home",
        icon: Icons.MaterialCommunityIcons.Home,
      },
      {
        text: "News",
        routeName: "NewsScreen",
        icon: Icons.MaterialCommunityIcons.News,
      },
      {
        text: "Publicites",
        routeName: "CustomPage",
        params: {
          id: 12612,
          title: "Adverts",
          url: "https://shop.wakeupprogramkingsfoundation-rdc.com/adverts"
        },
        icon: Icons.MaterialCommunityIcons.Pin,
      },
      {
        text: "Espace Vendeur",
        routeName: "CustomPage",
        params: {
          url: "https://shop.wakeupprogramkingsfoundation-rdc.com/dashboard",
        },
        icon: Icons.MaterialCommunityIcons.Email,
      },
      {
        text: "Posez Questions",
        routeName: "CustomPage",
        params: {
          title: "Posez Questions",
          url: "https://shop.wakeupprogramkingsfoundation-rdc.com/submit-ticket-2/"
        },
      }
    ],
  },

  // define menu for profile tab
  ProfileSettings: [
    {
      label: "WishList",
      routeName: "WishListScreen",
    },
    {
      label: "MyOrder",
      routeName: "MyOrders",
    },
    {
      label: "Address",
      routeName: "Address",
    },
    {
      label: "Currency",
      isActionSheet: true,
    },
    // only support mstore pro
    {
      label: "Languages",
      routeName: "SettingScreen",
    },
    {
      label: "PushNotification",
    },
    {
      label: "DarkTheme",
    },
    {
      label: "contactus",
      routeName: "Aboutus",
      params: {
        // id: 10941,
        // title: "contactus",
        // url: "https://shop.wakeupprogramkingsfoundation-rdc.com/contact-us",
      },
    },
    {
      label: "Privacy",
      routeName: "CustomPage",
      params: {
        url: "https://shop.wakeupprogramkingsfoundation-rdc.com/",
      },
    },
    {
      label: "termCondition",
      routeName: "Termsconditions",
      params: {
        // url: "https://shop.wakeupprogramkingsfoundation-rdc.com/terms-and-conditions/",
      },
    },
    {
      label: "About",
      routeName: "Aboutus",
      params: {
        // url: "https://shop.wakeupprogramkingsfoundation-rdc.com/",
      },
    },
  ],

  // Homepage Layout setting
  layouts: [
    {
      layout: Constants.Layout.card,
      image: Images.icons.iconCard,
      text: "cardView",
    },
    {
      layout: Constants.Layout.simple,
      image: Images.icons.iconRight,
      text: "simpleView",
    },
    {
      layout: Constants.Layout.twoColumn,
      image: Images.icons.iconColumn,
      text: "twoColumnView",
    },
    {
      layout: Constants.Layout.threeColumn,
      image: Images.icons.iconThree,
      text: "threeColumnView",
    },
    {
      layout: Constants.Layout.horizon,
      image: Images.icons.iconHorizal,
      text: "horizontal",
    },
    {
      layout: Constants.Layout.advance,
      image: Images.icons.iconAdvance,
      text: "advanceView",
    },
  ],

  // Default theme loading, this could able to change from the user profile (reserve feature)
  Theme: {
    isDark: false,
  },

  // new list category design
  CategoriesLayout: Constants.CategoriesLayout.card,

  // WARNING: Currently when you change DefaultCurrency, please uninstall your app and reinstall again. The redux saved store.
  DefaultCurrency: {
    symbol: "$",
    name: "US Dollar",
    code: "USD",
    name_plural: "US dollars",
    decimal: ".",
    thousand: ",",
    precision: 2,
    format: "%s%v", // %s is the symbol and %v is the value
  },

  DefaultCountry: {
    code: "en",
    RTL: false,
    language: "English",
    countryCode: "US",
    hideCountryList: false, // when this option is try we will hide the country list from the checkout page, default select by the above 'countryCode'
  },

  /**
   * Config notification onesignal, only effect for the Pro version
   */
  OneSignal: {
    appId: "43948a3d-da03-4e1a-aaf4-cb38f0d9ca51",
  },
  /**
   * Login required
   */
  Login: {
    RequiredLogin: false, // required before using the app
    AnonymousCheckout: false, // required before checkout or checkout anonymous
  },

  Layout: {
    HideHomeLogo: false,
    HideLayoutModal: false,
  },

  Affiliate: { enable: false },

  EnableOnePageCheckout: false,

  NativeOnePageCheckout: true,

  // using url from server to load AppConfig.json
  HomeCaching: {
    url: `https://demo.mstore.io/wp-json/wc/v2/flutter/cache?consumer_key=${consumerKey}&consumer_secret=${consumerSecret}`, // can change url to load another server
    enable: false, // disable load from server, and start load in local in `common/AppConfig.json`
  },
};
