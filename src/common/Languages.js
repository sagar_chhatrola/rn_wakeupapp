/** @format */

import LocalizedStrings from "react-native-localization";

export default new LocalizedStrings({
  en: {
    Exit: "Exit",
    ExitConfirm: "Are you sure you want to exit this app",
    YES: "YES",
    OK: "OK",
    ViewMyOrders: "View My Oders",
    CANCEL: "CANCEL",
    Confirm: "Confirm",

    // Scene's Titles
    Home: "Home",
    Intro: "Intro",
    Product: "Product",
    Cart: "Cart",
    WishList: "WishList",
    InputPhone: "Please input your phone",

    // Home
    products: "products",

    // TopBar
    ShowFilter: "Sub Categories",
    HideFilter: "Hide",
    Sort: "Sort",
    textFilter: "Recent",

    // Category
    ThereIsNoMore: "There is no more product to show",

    // Product
    AddedtoCart: "Added item to Cart",
    AddtoCart: "Add to Cart",
    AddtoWishlist: "Add to Wishlist",
    ProductVariations: "Variations",
    NoVariation: "This product don't have any variation",
    AdditionalInformation: "Description",
    NoProductDescription: "No Product Description",
    ProductReviews: "Reviews",
    NoReview: "This product don't have any reviews ...yet",
    BUYNOW: "BUY NOW",
    ProductLimitWaring: "You can't add more than 10 product",
    EmptyProductAttribute: "This product don't have any attributes",
    ProductFeatures: "Features",
    ErrorMessageRequest: "Can't get data from server",
    NoConnection: "No internet connection",
    ProductRelated: "You May Also Like",

    // Cart
    NoCartItem: "There is no product in cart",
    Total: "Total",
    EmptyCheckout: "Sorry, you can't check out an empty cart",
    RemoveCartItemConfirm: "Remove this product from cart?",
    MyCart: "Cart",
    Order: "Order",
    ShoppingCart: "Shopping Cart",
    ShoppingCartIsEmpty: "Your Cart is Empty",
    Delivery: "Delivery",
    AddProductToCart: "Add a product to the shopping cart",
    TotalPrice: "Total Price:",
    YourDeliveryInfo: "Your delivery info:",
    ShopNow: "Shop Now",
    YourChoice: "Your wishlist:",
    YourSale: "Your Sale:",
    SubtotalPrice: "Subtotal Price:",
    BuyNow: "Buy Now",
    Items: "items",
    Item: "item",
    ThankYou: "Thank you",
    FinishOrderCOD: "You can use to number of order to track shipping status",
    FinishOrder:
      "Thank you so much for your purchased, to check your delivery status please go to My Orders",
    NextStep: "Next Step",
    ConfirmOrder: "Confirm Order",
    RequireEnterAllFileds: "Please enter all fields",
    Error: "Error",
    InvalidEmail: "Invalid email address",
    Finish: "Finish",

    // Wishlist
    NoWishListItem: "There is no item in wishlist",
    MoveAllToCart: "Add all to cart",
    EmptyWishList: "Empty wishlist",
    EmptyAddToCart: "Sorry, the wishlist is empty",
    RemoveWishListItemConfirm: "Remove this product from wishlist?",
    CleanAll: "Clean All",

    // Sidemenu
    SignIn: "Log In",
    SignOut: "Log Out",
    GuestAccount: "Guest Account",
    CantReactEmailError:
      "We can't reach your email address, please try other login method",
    NoEmailError: "Your account don't have valid email address",
    EmailIsNotVerifiedError:
      "Your email address is not verified, we can' trust you",
    Shop: "Shop",
    News: "News",
    Contact: "Contact us",
    Setting: "Setting",
    Login: "Login",
    Logout: "Logout",
    Category: "Category",
    Adverts: "Classofied Ads",
    VendorDashboard: "Vendor Dashboard",

    // Checkout
    Checkout: "Checkout",
    ProceedPayment: "Proceed Payment",
    Purchase: "Purchase",
    CashOnDelivery: "Cash on Delivery",
    Paypal: "Paypal",
    Stripe: "Stripe",
    woocommerce_dpo: "woocommerce_dpo",
    maxicashgateway: "maxicashgateway",
    CreditCard: "Credit Card",
    PaymentMethod: "Payment Method - Not select",
    PaymentMethodError: "Please select your payment method",
    PayWithCoD: "Your purchase will be pay when goods were delivered",
    PayWithPayPal: "Your purchase will be pay with PayPal",
    PayWithStripe: "Your purchase will be pay with Stripe",
    ApplyCoupon: "Apply",
    CouponPlaceholder: "COUPON CODE",
    APPLY: "APPLY",
    Back: "Back",
    CardNamePlaceholder: "Name written on card",
    BackToHome: "Back to Home",
    OrderCompleted: "Your order was completed",
    OrderCanceled: "Your order was canceled",
    OrderFailed: "Something went wrong...",
    OrderCompletedDesc: "Your order id is ",
    OrderCanceledDesc:
      "You have canceled the order. The transaction has not been completed",
    OrderFailedDesc:
      "We have encountered an error while processing your order. The transaction has not been completed. Please try again",
    OrderTip:
      'Tip: You could track your order status in "My Orders" section from side menu',
    Payment: "Payment",
    Complete: "Complete",
    EnterYourFirstName: "Enter your First Name",
    EnterYourLastName: "Enter your Last Name",
    EnterYourEmail: "Enter your email",
    EnterYourPhone: "Enter your phone",
    EnterYourAddress: "Enter your address",
    CreateOrderError: "Cannot create new order. Please try again later",
    AccountNumner: "Account number",
    CardHolderName: "Cardholder Name",
    ExpirationDate: "Expiration Date",
    SecurityCode: "CVV",

    // myorder
    OrderId: "Order ID",
    MyOrder: "My Orders",
    NoOrder: "You don't have any orders",
    OrderDate: "Order Date: ",
    OrderStatus: "Status: ",
    OrderPayment: "Payment method: ",
    OrderTotal: "Total: ",
    OrderDetails: "Show detail",
    ShippingAddress: "Shipping Address:",
    Refund: "Refund",

    PostDetails: "Post Details",
    FeatureArticles: "Feature articles",
    MostViews: "Most views",
    EditorChoice: "Editor choice",

    // settings
    Settings: "Settings",
    BASICSETTINGS: "BASIC SETTINGS",
    Language: "Language",
    INFO: "INFO",
    About: "About us",
    changeRTL: "Switch RTL",

    // language
    AvailableLanguages: "Available Languages",
    SwitchLanguage: "Switch Language",
    SwitchLanguageConfirm: "Switch language require an app reload, continue?",
    SwitchRtlConfirm: "Switch RTL require an app reload, continue?",

    // about us
    AppName: "WakeupProgram",
    AppDescription: "Africa's Largest Marketplace",
    AppContact: " Contact us at: wakeupkingfoundation@gmail.com",
    AppEmail: " Email: wakeupkingfoundation@gmail.com",
    AppCopyRights: "© Wakeupking 2020",

    // contact us
    contactus: "Contact us",

    // form
    NotSelected: "Not selected",
    EmptyError: "This field is empty",
    DeliveryInfo: "Delivery Info",
    FirstName: "First Name",
    LastName: "Last Name",
    Address: "Address",
    City: "Town/City",
    State: "State",
    NotSelectedError: "Please choose one",
    Postcode: "Postcode",
    Country: "Country",
    Email: "Email",
    Phone: "Phone Number",
    Note: "Note",

    // search
    Search: "Search",
    SearchPlaceHolder: "Search product by name",
    NoResultError: "Your search keyword did not match any products.",
    Details: "Details",

    // filter panel
    Categories: "Categories",

    // sign up
    profileDetail: "Profile Details",
    firstName: "First name",
    lastName: "Last name",
    accountDetails: "Account Details",
    username: "Username",
    email: "Email",
    generatePass: "Use generate password",
    password: "Password",
    signup: "Sign Up",

    // filter panel
    Loading: "LOADING...",
    welcomeBack: "Welcome back! ",
    seeAll: "Show All",

    // Layout
    cardView: "Card ",
    simpleView: "List View",
    twoColumnView: "Two Column ",
    threeColumnView: "Three Column ",
    listView: "List View",
    default: "Default",
    advanceView: "Advance ",
    horizontal: "Horizontal ",

    couponCodeIsExpired: "This coupon code is expired",
    invalidCouponCode: "This coupon code is invalid",
    remove: "Remove",
    reload: "Reload",
    applyCouponSuccess: "Congratulations! Coupon code applied successfully ",
    applyCouponFailMin: `This minimum spend for this coupon is `,
    applyCouponFailMax: `This maximum spend for this coupon is `,

    OutOfStock: "OUT OF STOCK",
    ShippingType: "Shipping method",

    // Place holder
    TypeFirstName: "Type your first name",
    TypeLastName: "Type your last name",
    TypeAddress: "Type address",
    TypeCity: "Type your town or city",
    TypeState: "Type your state",
    TypeNotSelectedError: "Please choose one",
    TypePostcode: "Type postcode",
    TypeEmail: "Type email (Ex. acb@gmail.com), ",
    TypePhone: "Type your phone number",
    TypeNote: "Note",
    TypeCountry: "Select country",
    SelectPayment: "Select Payment method",
    close: "CLOSE",
    noConnection: "NO INTERNET ACCESS",

    // user profile screen
    AccountInformations: "Account Informations",
    PushNotification: "Push notification",
    DarkTheme: "Dark Theme",
    Privacy: "Privacy policies",
    SelectCurrency: "Select currency",
    Name: "Name",
    Currency: "Currency",
    Languages: "Languages",

    GetDataError: "Can't get data from server",
    UserOrEmail: "Username or email",
    Or: "Or",
    FacebookLogin: "Facebook Login",
    DontHaveAccount: "Don't have an account?",

    // Horizontal
    featureProducts: "Feature Products",
    bagsCollections: "Bags Collections",
    womanBestSeller: "Woman Best Seller",
    manCollections: "Man Collections",

    // Modal
    Select: "Select",
    Cancel: "Cancel",
    Guest: "Guest",

    LanguageName: "English",

    // review
    vendorTitle: "Vendor",
    comment: "Leave a review",
    yourcomment: "Your comment",
    placeComment:
      "Tell something about your experience or leave a tip for others",
    writeReview: "Review",
    thanksForReview:
      "Thanks for the review, your content will be verify by the admin and will be published later",
    errInputComment: "Please input your content to submit",
    errRatingComment: "Please rating to submit",
    send: "Send",

    termCondition: "Term & Condition",
    Subtotal: "Subtotal",
    Discount: "Discount",
    Shipping: "Shipping",
    Taxes: "Taxes",
    Recents: "Recents",
    Filters: "Filters",
    Princing: "Pricing",
    Filter: "Filter",
    ClearFilter: "Clear Filter",
    ProductCatalog: "Product Catalog",
    ProductTags: "Product Tags",
    AddToAddress: "Add to Address",
    SMSLogin: "SMS Login",
    OrderNotes: "Order Notes",

    CanNotLogin: "Can not login, something was wrong!",
    PleaseCompleteForm: "Please complete the form!",
    ServerNotResponse: "Server doesn't response correctly",
    CanNotRegister: "Can't register user, please try again."
  },
  fr: {
    Exit: "Sortie",
    ExitConfirm: "Êtes-vous sûr de vouloir quitter cette application",
    YES: "OUI",
    OK: "D'accord",
    ViewMyOrders: "Voir mes commandes",
    CANCEL: "ANNULER",
    Confirm: "Confirmer",

    // Scene's Titles
    Home: "Accueil",
    Intro: "Intro",
    Product: "Produit",
    Cart: "Chariot",
    WishList: "Liste de souhaits",
    InputPhone: "Veuillez saisir votre téléphone",

    // Home
    products: "des produits",

    // TopBar
    ShowFilter: "Sous-catégories",
    HideFilter: "Cacher",
    Sort: "Trier",
    textFilter: "Récente",

    // Category
    ThereIsNoMore: "Il n'y a plus de produit à montrer",

    // Product
    AddedtoCart: "Article ajouté au panier",
    AddtoCart: "Ajouter au chariot",
    AddtoWishlist: "Ajouter à la liste de souhaits",
    ProductVariations: "Variations",
    NoVariation: "Ce produit n'a aucune variation",
    AdditionalInformation: "La description",
    NoProductDescription: "Aucune description de produit",
    ProductReviews: "Commentaires",
    NoReview: "Ce produit n'a pas encore d'avis ...",
    BUYNOW: "ACHETER MAINTENANT",
    ProductLimitWaring: "Vous ne pouvez pas ajouter plus de 10 produits",
    EmptyProductAttribute: "Ce produit n'a aucun attribut",
    ProductFeatures: "Caractéristiques",
    ErrorMessageRequest: "Impossible d'obtenir les données du serveur",
    NoConnection: "Pas de connexion Internet",
    ProductRelated: "Tu pourrais aussi aimer",

    // Cart
    NoCartItem: "Il n'y a pas de produit dans le panier",
    Total: "Totale",
    EmptyCheckout: "Désolé, vous ne pouvez pas vérifier un panier vide",
    RemoveCartItemConfirm: "Supprimer ce produit du panier?",
    MyCart: "Chariot",
    Order: "Ordre",
    ShoppingCart: "Panier",
    ShoppingCartIsEmpty: "Votre panier est vide",
    Delivery: "Livraison",
    AddProductToCart: "Ajouter un produit au panier",
    TotalPrice: "Prix ​​total:",
    YourDeliveryInfo: "Vos informations de livraison:",
    ShopNow: "Achetez maintenant",
    YourChoice: "Votre liste de souhaits:",
    YourSale: "Votre vente:",
    SubtotalPrice: "Prix ​​sous-total:",
    BuyNow: "Acheter maintenant",
    Items: "articles",
    Item: "article",
    ThankYou: "Je vous remercie",
    FinishOrderCOD: "Vous pouvez utiliser le numéro de commande pour suivre l'état de l'expédition",
    FinishOrder:
      "Merci beaucoup pour votre achat, pour vérifier l'état de votre livraison, veuillez vous rendre dans Mes commandes",
    NextStep: "L'étape suivante",
    ConfirmOrder: "Confirmer la commande",
    RequireEnterAllFileds: "Veuillez saisir tous les champs",
    Error: "Error",
    InvalidEmail: "Adresse e-mail invalide",
    Finish: "Terminer",

    // Wishlist
    NoWishListItem: "Il n'y a aucun article dans la liste de souhaits",
    MoveAllToCart: "Tout ajouter au panier",
    EmptyWishList: "Liste de souhaits vide",
    EmptyAddToCart: "Désolé, la liste de souhaits est vide",
    RemoveWishListItemConfirm: "Supprimer ce produit de la liste de souhaits?",
    CleanAll: "Clean All",

    // Sidemenu
    SignIn: "s'identifier",
    SignOut: "Se déconnecter",
    GuestAccount: "Compte d'invité",
    CantReactEmailError:
      "Nous ne pouvons pas atteindre votre adresse e-mail, veuillez essayer une autre méthode de connexion",
    NoEmailError: "Votre compte n'a pas d'adresse e-mail valide",
    EmailIsNotVerifiedError:
      "Votre adresse e-mail n'est pas vérifiée, nous pouvons vous faire confiance",
    Shop: "Shop",
    News: "News",
    Contact: "Nous contacter",
    Setting: "Réglage",
    Login: "S'identifier",
    Logout: "Se déconnecter",
    Category: "Catégorie",
    Adverts: "Petites annonces",
    VendorDashboard: "Tableau de bord du fournisseur",

    // Checkout
    Checkout: "Checkout",
    ProceedPayment: "Procéder au paiement",
    Purchase: "Achat",
    CashOnDelivery: "Cash on Delivery",
    Paypal: "Paypal",
    Stripe: "Stripe",
    woocommerce_dpo: "woocommerce_dpo",
    maxicashgateway: "maxicashgateway",
    CreditCard: "Credit Card",
    PaymentMethod: "Payment Method - Not select",
    PaymentMethodError: "Veuillez sélectionner votre mode de paiement",
    PayWithCoD: "Votre achat sera payé lors de la livraison des marchandises",
    PayWithPayPal: "Votre achat sera payé avec PayPal",
    PayWithStripe: "Votre achat sera payé avec Stripe",
    ApplyCoupon: "Apply",
    CouponPlaceholder: "COUPON CODE",
    APPLY: "APPLIQUER",
    Back: "Arrière",
    CardNamePlaceholder: "Nom inscrit sur la carte",
    BackToHome: "Back to Home",
    OrderCompleted: "Votre commande est terminée",
    OrderCanceled: "Votre commande a été annulée",
    OrderFailed: "Something went wrong...",
    OrderCompletedDesc: "Votre identifiant de commande est",
    OrderCanceledDesc:
      "Vous avez annulé la commande. La transaction n'est pas terminée",
    OrderFailedDesc:
      "Nous avons rencontré une erreur lors du traitement de votre commande. La transaction n'est pas terminée. Veuillez réessayer",
    OrderTip:
      'Tip: You could track your order status in "My Orders" section from side menu',
    Payment: "Payment",
    Complete: "Complete",
    EnterYourFirstName: "Entrez votre prénom",
    EnterYourLastName: "Entrez votre nom",
    EnterYourEmail: "Entrer votre Email",
    EnterYourPhone: "Entrez votre téléphone",
    EnterYourAddress: "Entrez votre adresse",
    CreateOrderError: "Impossible de créer une nouvelle commande. Veuillez réessayer plus tard",
    AccountNumner: "Account number",
    CardHolderName: "Cardholder Name",
    ExpirationDate: "Expiration Date",
    SecurityCode: "CVV",

    // myorder
    OrderId: "Order ID",
    MyOrder: "Mes Ordres",
    NoOrder: "Vous n'avez aucune commande",
    OrderDate: "Date de commande:",
    OrderStatus: "Statut: ",
    OrderPayment: "Mode de paiement: ",
    OrderTotal: "Totale: ",
    OrderDetails: "Montrer les détails",
    ShippingAddress: "Adresse de livraison:",
    Refund: "Rembourser",

    PostDetails: "Détails de l'article",
    FeatureArticles: "Articles de fond",
    MostViews: "Les plus vus",
    EditorChoice: "Choix de l'éditeur",

    // settings
    Settings: "Settings",
    BASICSETTINGS: "BASIC SETTINGS",
    Language: "Langue",
    INFO: "INFO",
    About: "À propos de nous",
    changeRTL: "Switch RTL",

    // language
    AvailableLanguages: "Available Languages",
    SwitchLanguage: "Changer de langue",
    SwitchLanguageConfirm: "Changer de langue nécessite un rechargement de l'application, continuer?",
    SwitchRtlConfirm: "Switch RTL require an app reload, continue?",

    // about us
    AppName: "WakeupProgram",
    AppDescription: "Africa's Largest Marketplace",
    AppContact: " Contact us at: wakeupkingfoundation@gmail.com",
    AppEmail: " Email: wakeupkingfoundation@gmail.com",
    AppCopyRights: "© Wakeupking 2020",

    // contact us
    contactus: "Nous contacter",

    // form
    NotSelected: "Non séléctionné",
    EmptyError: "Ce champ est vide",
    DeliveryInfo: "Informations de livraison",
    FirstName: "First Name",
    LastName: "Last Name",
    Address: "Address",
    City: "Town/City",
    State: "State",
    NotSelectedError: "Please choose one",
    Postcode: "Postcode",
    Country: "Country",
    Email: "Email",
    Phone: "Phone Number",
    Note: "Note",

    // search
    Search: "Search",
    SearchPlaceHolder: "Rechercher un produit par nom",
    NoResultError: "Votre mot clé de recherche ne correspond à aucun produit.",
    Details: "Détails",

    // filter panel
    Categories: "Categories",

    // sign up
    profileDetail: "Détails du profil",
    firstName: "First name",
    lastName: "Last name",
    accountDetails: "Détails du compte",
    username: "Username",
    email: "Email",
    generatePass: "Use generate password",
    password: "Password",
    signup: "Sign Up",

    // filter panel
    Loading: "LOADING...",
    welcomeBack: "Nous saluons le retour! ",
    seeAll: "Voir Plus",

    // Layout
    cardView: "Card ",
    simpleView: "List View",
    twoColumnView: "Two Column ",
    threeColumnView: "Three Column ",
    listView: "List View",
    default: "Default",
    advanceView: "Advance ",
    horizontal: "Horizontal ",

    couponCodeIsExpired: "Ce code de réduction est expiré",
    invalidCouponCode: "Ce code de réduction n'est pas valide",
    remove: "Retirer",
    reload: "Recharger",
    applyCouponSuccess: "Toutes nos félicitations! Code de coupon appliqué avec succès ",
    applyCouponFailMin: `This minimum spend for this coupon is `,
    applyCouponFailMax: `This maximum spend for this coupon is `,

    OutOfStock: "EN RUPTURE DE STOCK",
    ShippingType: "Mode de livraison",

    // Place holder
    TypeFirstName: "Tapez votre prénom",
    TypeLastName: "Tapez votre nom",
    TypeAddress: "Saisissez l'adresse",
    TypeCity: "Tapez votre ville ou ville",
    TypeState: "Tapez votre état",
    TypeNotSelectedError: "Choisissez en un s'il vous plait",
    TypePostcode: "Tapez le code postal",
    TypeEmail: "Saisissez un e-mail (Ex. acb@gmail.com), ",
    TypePhone: "Tapez votre numéro de téléphone",
    TypeNote: "Note",
    TypeCountry: "Choisissez le pays",
    SelectPayment: "Sélectionnez le mode de paiement",
    close: "CLOSE",
    noConnection: "NO INTERNET ACCESS",

    // user profile screen
    AccountInformations: "Informations sur le compte",
    PushNotification: "Notification push",
    DarkTheme: "Dark Theme",
    Privacy: "Les politiques de confidentialité",
    SelectCurrency: "Sélectionnez la devise",
    Name: "Nom",
    Currency: "Devise",
    Languages: "Langues",

    GetDataError: "Impossible d'obtenir les données du serveur",
    UserOrEmail: "Username or email",
    Or: "Or",
    FacebookLogin: "Facebook Login",
    DontHaveAccount: "Don't have an account?",

    // Horizontal
    featureProducts: "Produits caractéristiques",
    bagsCollections: "Bags Collections",
    womanBestSeller: "Woman Best Seller",
    manCollections: "Man Collections",

    // Modal
    Select: "Select",
    Cancel: "Cancel",
    Guest: "Guest",

    LanguageName: "French",

    // review
    vendorTitle: "Vendor",
    comment: "Leave a review",
    yourcomment: "Your comment",
    placeComment:
      "Tell something about your experience or leave a tip for others",
    writeReview: "Review",
    thanksForReview:
      "Merci pour l'examen, votre contenu sera vérifié par l'administrateur et sera publié plus tard",
    errInputComment: "Please input your content to submit",
    errRatingComment: "Please rating to submit",
    send: "Send",

    termCondition: "Terme et condition",
    Subtotal: "Subtotal",
    Discount: "Discount",
    Shipping: "Shipping",
    Taxes: "Taxes",
    Recents: "Recents",
    Filters: "Filters",
    Princing: "Pricing",
    Filter: "Filter",
    ClearFilter: "Clear Filter",
    ProductCatalog: "Product Catalog",
    ProductTags: "Product Tags",
    AddToAddress: "Add to Address",
    SMSLogin: "SMS Login",
    OrderNotes: "Order Notes",

    CanNotLogin: "Can not login, something was wrong!",
    PleaseCompleteForm: "Veuillez remplir le formulaire!",
    ServerNotResponse: "Le serveur ne répond pas correctement",
    CanNotRegister: "Impossible d'enregistrer l'utilisateur, veuillez réessayer."
  },
});
